#!/usr/bin/env bash
log() {
	echo "=== $1"
}

REGISTRATOR_TAG=v4
SWARM_MASTER=swarm-master
CONSUL_MASTER=$SWARM_MASTER
LOAD_BALANCER=load-balancer
SERVER_SESSION=redis-server

# create swarm token
log "Creating Swarm token"
docker-machine create -d virtualbox local
eval "$(docker-machine env local)"
SWARM_TOKEN=$(docker run swarm create)

log "Swarm token is: $SWARM_TOKEN"

# create Swarm Master
log "Creating Swarm master"
docker-machine create \
    -d virtualbox \
    --swarm \
    --swarm-master \
    --swarm-discovery token://$SWARM_TOKEN \
    $SWARM_MASTER

log "Starting Consul in Swarm Master"

# bootstramp Consul master, that all othe consuls running in the nodes will join
#log "Creating Consul Master node"
#docker-machine create \
#	    -d virtualbox \
#	    $CONSUL_MASTER

eval "$(docker-machine env $CONSUL_MASTER)"
CONSUL_MASTER_IP=$(docker-machine ip $CONSUL_MASTER)
docker run -d --name $CONSUL_MASTER -h $CONSUL_MASTER \
    -p $CONSUL_MASTER_IP:8300:8300 \
    -p $CONSUL_MASTER_IP:8301:8301 \
    -p $CONSUL_MASTER_IP:8301:8301/udp \
    -p $CONSUL_MASTER_IP:8302:8302 \
    -p $CONSUL_MASTER_IP:8302:8302/udp \
    -p $CONSUL_MASTER_IP:8400:8400 \
    -p $CONSUL_MASTER_IP:8500:8500 \
    -p $CONSUL_MASTER_IP:53:53 \
    -p $CONSUL_MASTER_IP:53:53/udp \
    progrium/consul \
    -server \
    -advertise $(docker-machine ip $CONSUL_MASTER) \
    -bootstrap

log "Consul Master IP address: $CONSUL_MASTER_IP"

log "Starting registrator in Swarm master node"
eval "$(docker-machine env $SWARM_MASTER)"
#SWARM_MASTER_IP=$(docker-machine ip $SWARM_MASTER)
docker run -d \
	-v /var/run/docker.sock:/tmp/docker.sock \
	-h registrator-swarm-master \
	--name registrator-swarm-master \
	gliderlabs/registrator:$REGISTRATOR_TAG \
	consul://$(docker-machine ip $SWARM_MASTER):8500 \
	-ip $(docker-machine ip $SWARM_MASTER)

# create Swarm nodes
SWARM_NODES=("node-01", "node-02", "node-03")
docker build -t mynode /var/www/load_balancing/nodejs/
for i in "${SWARM_NODES[@]}"; do
	log "Creating Swarm node $i"
	docker-machine create \
	    -d virtualbox \
	    --swarm \
	    --swarm-discovery token://$SWARM_TOKEN \
	    $i

    eval "$(docker-machine env $i)"
	NODE_IP=$(docker-machine ip $i)
    docker run --name consul-$i -d -h $i \
        -p $NODE_IP:8300:8300 \
        -p $NODE_IP:8301:8301 \
        -p $NODE_IP:8301:8301/udp \
        -p $NODE_IP:8302:8302 \
        -p $NODE_IP:8302:8302/udp \
        -p $NODE_IP:8400:8400 \
        -p $NODE_IP:8500:8500 \
        -p $NODE_IP:53:53 \
        -p $NODE_IP:53:53/udp \
        progrium/consul \
        -server \
        -advertise $NODE_IP \
        -join $CONSUL_MASTER_IP

	log "Starting Registrator in node $i"

    docker run -d \
        -v /var/run/docker.sock:/tmp/docker.sock \
        -h registrator-node-$i \
        --name registrator-node-$i \
        gliderlabs/registrator:$REGISTRATOR_TAG \
        consul://$NODE_IP:8500 \
        -ip $NODE_IP

	# Run the local nginx

	docker run \
		-d \
		-h nginx-node-$i \
		-e SERVICE_NAME=rest \
		--name=nginx-node-$i \
		--dns $NODE_IP \
		-p 80:80 \
		-p 1936:1936 \
		nginx

    docker run \
		-d \
		-h redis-node-$i \
		-e SERVICE_NAME=nosql_db \
		--name=redis-node-$i \
		--dns $NODE_IP \
		-p 6379:6379 \
		redis


    docker run \
		-d \
		-h postgre-node-$i \
		-e SERVICE_NAME=db \
		--name=postgre-node-$i \
		--dns $NODE_IP \
		-p 5432:5432 \
		postgres

    docker run \
        -d \
        -h nodejs-node-$i \
        -v /var/www/load_balancing/nodejs/:/usr/src/app \
        -e SERVICE_NAME=distributed_app \
        --name=nodeJS-node-$i \
        --dns $NODE_IP \
        -p 3000:3000 \
        mynode

done

# start the example service, in *any* of the nodes; unfortunately, docker-machine does not support
# setting Swarm labels yet so this container could end up in the consul-master node
eval "$(docker-machine env --swarm $SWARM_MASTER)"
#SERVICE=("service-01" "service-02")
SERVICES=("service-01")
for i in "${SERVICES[@]}"; do
	docker run \
	        -d \
		-e SERVICE_NAME=hello/v1 \
		-e SERVICE_TAGS=rest \
		-h kitematic-$i \
		--name kitematic-$i \
		-p :80 \
		kitematic/hello-world-nginx

	docker run \
        	-d \
                -e SERVICE_NAME=hello/v2 \
                -e SERVICE_TAGS=rest \
                -h hello-$i \
                --name hello-$i \
                -p :80 \
		sirile/scala-boot-test
	docker run \
            -d \
		    -v /var/www/load_balancing/nginx/2048-master:/website_files \
                -e SERVICE_NAME=game/2048 \
                -e SERVICE_TAGS=rest \
                -h game-2048 \
                --name game-2048-$i \
                -p :80 \
		kitematic/hello-world-nginx
done

#log "Creating server session"
#docker-machine create \
#	-d virtualbox \
#	$SERVER_SESSION

# start the server session
#eval "$(docker-machine env $SERVER_SESSION)"
#SERVER_SESSION_IP=$(docker-machine ip $SERVER_SESSION)
#docker run \
#	-d \
#	-h redis-server-session \
#	-e SERVICE_NAME=nosql_db \
#	--name=$SERVER_SESSION \
#	--dns $SERVER_SESSION_IP \
#	-p 6379:6379 \
#	redis

#docker run -d --name CONSUL_SERVER_SESSION -h $SERVER_SESSION \
#	-p $SERVER_SESSION_IP:8300:8300 \
#	-p $SERVER_SESSION_IP:8301:8301 \
#	-p $SERVER_SESSION_IP:8301:8301/udp \
#	-p $SERVER_SESSION_IP:8302:8302 \
#	-p $SERVER_SESSION_IP:8302:8302/udp \
#	-p $SERVER_SESSION_IP:8400:8400 \
#	-p $SERVER_SESSION_IP:8500:8500 \
#	-p $SERVER_SESSION_IP:53:53 \
#	-p $SERVER_SESSION_IP:53:53/udp \
#	progrium/consul \
#	-server \
#	-advertise $(docker-machine ip $SERVER_SESSION) \
#	-bootstrap \
#       -join $CONSUL_MASTER_IP


#docker run -d \
#	-v /var/run/docker.sock:/tmp/docker.sock \
#	-h registrator-server-session \
#	--name registrator-server-session \
#	gliderlabs/registrator:$REGISTRATOR_TAG \
#	consul://$SERVER_SESSION_IP:8500 \
#	-ip $SERVER_SESSION_IP



# Create the reverse proxy and load balancer
log "Creating load balancer"
docker-machine create \
	-d virtualbox \
	$LOAD_BALANCER

eval "$(docker-machine env $LOAD_BALANCER)"
#LOAD_BALANCER_IP=$(docker-machine ip $LOAD_BALANCER)

# start the load balancer
docker run \
	-d \
	-e SERVICE_NAME=rest \
	--name=$LOAD_BALANCER \
	--dns $CONSUL_MASTER_IP \
	-p 80:80 \
	-p 1936:1936 \
	sirile/haproxy

docker run -d --name CONSUL_LOAD_BALANCE -h CONSUL_LOAD_BALANCE \
	-p $CONSUL_MASTER_IP:8300:8300 \
	-p $CONSUL_MASTER_IP:8301:8301 \
	-p $CONSUL_MASTER_IP:8301:8301/udp \
	-p $CONSUL_MASTER_IP:8302:8302 \
	-p $CONSUL_MASTER_IP:8302:8302/udp \
	-p $CONSUL_MASTER_IP:8400:8400 \
	-p $CONSUL_MASTER_IP:8500:8500 \
	-p $CONSUL_MASTER_IP:53:53 \
	-p $CONSUL_MASTER_IP:53:53/udp \
	progrium/consul \
	-server \
	-advertise $(docker-machine ip $CONSUL_MASTER) \
	-bootstrap

docker run -d \
	-v /var/run/docker.sock:/tmp/docker.sock \
	-h load-balancer \
	--name registrator-load-balancer \
	gliderlabs/registrator:$REGISTRATOR_TAG \
	consul://$LOAD_BALANCER_IP:8500 \
	-ip $LOAD_BALANCER_IP

#HOST_MACHINE = host-machine
# Create host-machine
#docker-machine create \
#	-d virtualbox \
#	$HOST_MACHINE

#eval "$(docker-machine env $HOST_MACHINE)"
#HOST_MACHINE_IP=$(docker-machine ip $HOST_MACHINE)

#docker run -d --name CONSUL_HOST_MACHINE -h $HOST_MACHINE \
#	-p $HOST_MACHINE_IP:8300:8300 \
#	-p $HOST_MACHINE_IP:8301:8301 \
#	-p $HOST_MACHINE_IP:8301:8301/udp \
#	-p $HOST_MACHINE_IP:8302:8302 \
#	-p $HOST_MACHINE_IP:8302:8302/udp \
#	-p $HOST_MACHINE_IP:8400:8400 \
#	-p $HOST_MACHINE_IP:8500:8500 \
#	-p $HOST_MACHINE_IP:53:53 \
#	-p $HOST_MACHINE_IP:53:53/udp \
#	progrium/consul \
#	-server \
#	-advertise $(docker-machine ip $CONSUL_MASTER) \
#	-bootstrap

#docker run -d \
#	-v /var/run/docker.sock:/tmp/docker.sock \
#	-h load-balancer \
#	--name registrator-host-machine \
#	gliderlabs/registrator:$REGISTRATOR_TAG \
#	consul://$HOST_MACHINE_IP:8500 \
#	-ip $HOST_MACHINE_IP

#docker run \
#    -d \
#    -h host-nodejs \
#    -e SERVICE_NAME=distributed_app \
#    --name=nodeJS \
#    --dns $HOST_MACHINE_IP \
#    -p 3000:3000 \
#    node

# DND server, if it`s necessary
#DNS_SERVER=dns-server
#docker-machine create \
#	-d virtualbox \
#	$DNS_SERVER
#
#eval "$(docker-machine env $DNS_SERVER)"
#DNS_SERVER_IP=$(docker-machine ip $DNS_SERVER)
#
#docker run -d --name CONSUL_DNS_SERVER -h $DNS_SERVER \
#	-p $DNS_SERVER_IP:8300:8300 \
#	-p $DNS_SERVER_IP:8301:8301 \
#	-p $DNS_SERVER_IP:8301:8301/udp \
#	-p $DNS_SERVER_IP:8302:8302 \
#	-p $DNS_SERVER_IP:8302:8302/udp \
#	-p $DNS_SERVER_IP:8400:8400 \
#	-p $DNS_SERVER_IP:8500:8500 \
#	-p $DNS_SERVER_IP:53:53 \
#	-p $DNS_SERVER_IP:53:53/udp \
#	progrium/consul \
#	-server \
#	-advertise $(docker-machine ip $CONSUL_MASTER) \
#	-bootstrap
#
#docker run -d \
#	-v /var/run/docker.sock:/tmp/docker.sock \
#	-h load-balancer \
#	--name registrator-host-machine \
#	gliderlabs/registrator:$REGISTRATOR_TAG \
#	consul://$DNS_SERVER_IP:8500 \
